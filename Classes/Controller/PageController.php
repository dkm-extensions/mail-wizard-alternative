<?php

namespace DKM\MailWizardAlt\Controller;

use Psr\Http\Message\ServerRequestInterface;
use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\DataHandling\DataHandler;
use TYPO3\CMS\Core\Http\RedirectResponse;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class PageController
{
    public function createAction(ServerRequestInterface $serverRequest): RedirectResponse
    {
        $params = [];
        parse_str($serverRequest->getBody()->getContents(), $params);
        $dataHandler = GeneralUtility::makeInstance(DataHandler::class);
        if(($params['page'] ?? false) && ($params['inputNameNewInternalPage'] ?? false)) {
            $data['pages']['NEW83923be82'] = [
                'pid' => $params['page'],
                'title' => $params['inputNameNewInternalPage'],
                'doktype' => 24,
                'fe_group' => ''
            ];
            $dataHandler->start($data, []);
            $dataHandler->process_datamap();
        }
        return new RedirectResponse(current($GLOBALS['TYPO3_REQUEST']->getHeader('referer')));
    }

    public function copyAction(ServerRequestInterface $serverRequest): RedirectResponse
    {
        $pageToCopy = $serverRequest->getQueryParams()['page'];
        if($pageToCopy = BackendUtility::getRecord('pages', $serverRequest->getQueryParams()['page'] ?? false)) {
            $dataHandler = GeneralUtility::makeInstance(DataHandler::class);
            $cmd['pages'][$pageToCopy['uid']]['copy'] = $pageToCopy['pid'];
            $dataHandler->start([], $cmd);
            $dataHandler->process_cmdmap();
            $uid = $dataHandler->copyMappingArray_merged['pages'][$pageToCopy['uid']];

            $data['pages'][$uid] = [
                'title' => $pageToCopy['title'] . ' (kopi)'
            ];
            $dataHandler->start($data, []);
            $dataHandler->process_datamap();
        };
        return new RedirectResponse(current($GLOBALS['TYPO3_REQUEST']->getHeader('referer')));
    }

    public function deleteAction(ServerRequestInterface $serverRequest): RedirectResponse
    {
        if($pageToDelete = $serverRequest->getQueryParams()['page']) {
            $dataHandler = GeneralUtility::makeInstance(DataHandler::class);
            $cmd['pages'][$pageToDelete]['delete'] = 1;
            $dataHandler->start([], $cmd);
            $dataHandler->process_cmdmap();
        };
        return new RedirectResponse(current($GLOBALS['TYPO3_REQUEST']->getHeader('referer')));
    }
}