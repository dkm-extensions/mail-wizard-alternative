<?php

namespace DKM\MailWizardAlt\Hooks;

use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class DataHandler
{
    /**
     * @param $status
     * @param $table
     * @param $id
     * @param $fieldArray
     * @param $dataHandler
     * @throws \TYPO3\CMS\Core\Exception
     */
    public function processDatamap_afterDatabaseOperations($status, $table, $id, $fieldArray, $dataHandler) {
        if( in_array($table, ['tx_mail_domain_model_group', 'fe_groups']) ) {
            if($table == 'tx_mail_domain_model_group') {
                $match = ['uid_local' => $id];
                $select = ['uid_foreign'];
                $updateTable = 'fe_groups';
            } else {
                $match = ['uid_foreign' => $id];
                $select = ['uid_local'];
                $updateTable = 'tx_mail_domain_model_group';

            }
            $connection = GeneralUtility::makeInstance(ConnectionPool::class)->getConnectionForTable('tx_mail_group_mm');
            $records = $connection->select($select, 'tx_mail_group_mm', $match)->fetchFirstColumn();

            // Update title field of corresponding fe_groups
            $connection = GeneralUtility::makeInstance(ConnectionPool::class)->getConnectionForTable($updateTable);
            foreach ($records as $recordUid) {
                $connection->update($updateTable, ['title' => $dataHandler->datamap[$table][$id]['title']], ['uid' => $recordUid, 'title' => ($dataHandler->checkValue_currentRecord['title'] ?? time())]);
            }
        }
    }

}