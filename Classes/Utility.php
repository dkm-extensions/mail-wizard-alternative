<?php

namespace DKM\MailWizardAlt;

use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\DataHandling\DataHandler;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class Utility
{
    static public function createRecipientList($pid, $title): void
    {
        $dataHandler = GeneralUtility::makeInstance(DataHandler::class);
        $data = ['fe_groups' =>
            ['NEW83923be82' => [
                'pid' => $pid,
                'title' => $title]
            ]
        ];
        $dataHandler->start($data, []);
        $dataHandler->process_datamap();
        $fe_groups_uid = $dataHandler->substNEWwithIDs['NEW83923be82'] ?? false;

        self::createAndLinkMailGroup('fe_groups', $fe_groups_uid, $title, $pid);
    }

    /**
     * @param $tableName
     * @param $recordUid
     * @param $title
     * @param $pageUid
     */
    static public function createAndLinkMailGroup($tableName, $recordUid, $title, $pageUid): void
    {
        $mail_group_uid = static::insertRecord('tx_mail_domain_model_group',
            [
                'pid' => $pageUid,
                'tstamp' => time(),
                'deleted' => 0,
                'hidden' => 0,
                'type' => 2,
                'title' => $title,
                'description' => '',
                'static_list' => 1,
                'csv' => 0,
                'mail_html' => 1,
                'pages' => '',
                'recipient_sources' => '',
                'recursive' => 0,
                'children' => null,
                'categories' => 0
            ]
        );

        if ($recordUid && $mail_group_uid) {
            // Create relation
            static::insertRecord('tx_mail_group_mm',
                [
                    'uid_local' => $mail_group_uid,
                    'uid_foreign' => $recordUid,
                    'tablenames' => $tableName,
                    'sorting' => 0,
                    'sorting_foreign' => 0
                ]
            );
        }
    }
    /**
     * @param $table
     */
    static public function insertRecord($table, array $data = []): int
    {
        $connection = GeneralUtility::makeInstance(ConnectionPool::class)->getConnectionForTable($table);
        $connection->insert($table, $data);
        return (int)$connection->lastInsertId($table);
    }

}