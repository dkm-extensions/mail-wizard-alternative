<?php

namespace DKM\MailWizardAlt\ViewHelpers;

use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\ArrayUtility;
use TYPO3\CMS\Core\Utility\Exception\MissingArrayPathException;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;

class GetGlobalsVarViewHelper extends \TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper
{

    protected $escapeOutput = false;

    public function initializeArguments()
    {
        $this->registerArgument('path', 'string', 'Globals var path', true);
    }

    public static function renderStatic(array $arguments, \Closure $renderChildrenClosure, RenderingContextInterface $renderingContext)
    {
        try {
            return ArrayUtility::getValueByPath($GLOBALS, $arguments['path'], '.');
        } catch (MissingArrayPathException $e) {
            return '';
        }
    }

}