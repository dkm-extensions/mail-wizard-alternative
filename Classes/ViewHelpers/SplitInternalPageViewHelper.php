<?php

namespace DKM\MailWizardAlt\ViewHelpers;

use Doctrine\DBAL\Driver\Exception;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class SplitInternalPageViewHelper extends \TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper
{

    protected $escapeOutput = false;

    public function initializeArguments()
    {
        $this->registerArgument('pages', 'array', 'Array of internal pages', true);
    }

    /**
     * @throws Exception
     */
    public function render()
    {
        $pages = $this->arguments['pages'] ?? [];
        // Key is page id and value is if sent value (0 or 1)
        $mailPages = $this->getMailPages();

//        $sentMailPages = array_intersect($mailPages, [1]);
        $sentMailPages = array_filter($mailPages);

        // Pages which have already been sent
        $completed = array_intersect_key($pages, $sentMailPages);

        // Draft pages - pages not sent or staged to be sent yet
        $draftPages = array_diff_key($pages, $mailPages);
        $this->templateVariableContainer->add('draftPages', ['data' => $draftPages]);
        $this->templateVariableContainer->add('completed', ['data' => $completed]);
        return $this->renderChildren();
    }

    /**
     * Get pages which are staged as mail, where the key is the page id and the value is true if the mail is sent
     * @param $pages
     * @return mixed[]
     * @throws Exception
     */
    private function getMailPages(): array
    {
        return GeneralUtility::makeInstance(ConnectionPool::class)->getConnectionForTable('tx_mail_domain_model_mail')
        ->select(['page','sent'], 'tx_mail_domain_model_mail', ['deleted' => 0], ['page'])->fetchAllKeyValue();
    }


}