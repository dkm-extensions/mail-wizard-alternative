<?php
namespace DKM\MailWizardAlt\ViewHelpers;


use TYPO3\CMS\Core\Utility\ArrayUtility;
use TYPO3\CMS\Core\Utility\Exception\MissingArrayPathException;
use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;
use TYPO3Fluid\Fluid\Core\ViewHelper\Traits\CompileWithRenderStatic;

class UpdateArrayViewHelper extends AbstractViewHelper
{
    use CompileWithRenderStatic;
    /**
     * Initialize arguments.
     *
     * @throws \TYPO3Fluid\Fluid\Core\ViewHelper\Exception
     */
    public function initializeArguments()
    {
        $this->registerArgument('path', 'string', 'constant path', true);
        $this->registerArgument('newValue', 'mixed', '', true);
        $this->registerArgument('overrideOnly', 'bool', '', false, false);
    }


    /**
     * Renders the website title
     *
     * @return string
     * @throws \TYPO3\CMS\Fluid\Core\ViewHelper\Exception
     */
    public static function renderStatic(array $arguments, \Closure $renderChildrenClosure, RenderingContextInterface $renderingContext) {
        $allVariables = $renderingContext->getVariableProvider()->getAll();
        $getValueByPath = function($allVariables, $path) {
            try {
                return ArrayUtility::getValueByPath($allVariables, $path, '.');
            } catch (MissingArrayPathException $e) {
                return false;
            }
        };
        if(!$arguments['overrideOnly'] || $getValueByPath($allVariables, $arguments['path'])) {
            $allVariables = ArrayUtility::setValueByPath($allVariables, $arguments['path'], $arguments['newValue'], '.');
        }

        $renderingContext->getVariableProvider()->setSource($allVariables);

    }
}