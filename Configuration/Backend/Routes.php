<?php
return [
    'mail_wizard_alt_create_new_page' => [
        'path' => '/mail_wizard_alt/create_new_page',
        'target' => \DKM\MailWizardAlt\Controller\PageController::class . '::createAction'
    ],
    'mail_wizard_alt_create_new_page_from' => [
        'path' => '/mail_wizard_alt/create_new_page_from',
        'target' => \DKM\MailWizardAlt\Controller\PageController::class . '::copyAction'
    ],
    'mail_wizard_alt_delete_page' => [
        'path' => '/mail_wizard_alt/delete_page',
        'target' => \DKM\MailWizardAlt\Controller\PageController::class . '::deleteAction'
    ],
    'mail_wizard_alt_create_new_list' => [
        'path' => '/mail_wizard_alt/create_new_list',
        'target' => \DKM\MailWizardAlt\Controller\RecipientController::class . '::createAction'
    ],
    'mail_wizard_alt_create_new_list_from_medarbeideren' => [
        'path' => '/mail_wizard_alt/create_new_list_from',
        'target' => \DKM\MailWizardAlt\Controller\RecipientController::class . '::createFromMedarbeiderenAction'
    ],
    'mail_wizard_alt_delete_list' => [
        'path' => '/mail_wizard_alt/delete_list',
        'target' => \DKM\MailWizardAlt\Controller\RecipientController::class . '::deleteAction'
    ],
];