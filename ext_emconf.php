<?php

/* * *************************************************************
 * Extension Manager/Repository config file for ext "cdsrc_bepwreset".
 *
 * Auto generated 01-02-2015 10:24
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 * ************************************************************* */

$EM_CONF[$_EXTKEY] = [
    'title' => 'Alternative Wizard Configuration for EXT:Mail',
    'description' => 'Alternative wizard configuration for EXT:mail',
    'category' => 'Backend',
    'version' => '1.0.0',
    'state' => 'alpha',
    'author' => 'Stig Nørgaard Færch',
    'author_email' => 'snf@dkm.dk',
    'author_company' => 'Danmarks Kirkelige Mediecenter'
];

