<?php

$GLOBALS['TYPO3_CONF_VARS']['SYS']['locallangXMLOverride']['da']['EXT:mail/Resources/Private/Language/Modules.xlf'][] = 'EXT:mail_wizard_alt/Resources/Private/Language/da.Modules.xlf';
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_tcemain.php']['processDatamapClass']['mail_wizard_alt_rename'] = \DKM\MailWizardAlt\Hooks\DataHandler::class;
$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\MEDIAESSENZ\Mail\Controller\RecipientController::class] = [
    'className' => \DKM\MailWizardAlt\Controller\RecipientController::class,
];
$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\MEDIAESSENZ\Mail\Service\RecipientService::class] = [
    'className' => \DKM\MailWizardAlt\Service\RecipientService::class,
];